/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PongJavaFx;


import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Stage;
/**
 *
 * @author 1Daw02
 */
public class PongJavaFx extends Application {
  
    int direccionHorizontal = 2;
    int direccionVertical = 2;
    final int WORLD_WIDTH = 500;
    final int WORLD_HEIGHT = 400;
    final int ANCHO_JUGADOR = 5;
    final int ALTO_JUGADOR = 30;
    final int POS_X_JUGADOR1 = 5;
    final int POS_Y_JUGADOR1 = 180;
    final int POS_X_JUGADOR2 = 490;
    final int POS_Y_JUGADOR2 = 180;
    final int RADIO_BOLA = 5;
    double jugadorDerecho = 0;
    double  gravedadJugador1 = 0;
    double gravedadJugador2 = 0;
    final int DIR_X_BOLA = 250; 
    final int DIR_Y_BOLA = 200;
    short lineaX = 250;
    short lineaY = 0;
    short anchoLineaCentro = 3;
    short AltoLineaCentro = 400;
    int puntuacionJugador1 = 1;
    int puntuacionJugador2 = 1;
    

    
    @Override
    public void start(Stage primaryStage) {
        Group root = new Group();
        Scene scene = new Scene(root, WORLD_WIDTH, WORLD_HEIGHT, Color.SPRINGGREEN);
        primaryStage.setTitle("PongJavaFx");
        primaryStage.setScene(scene);
        primaryStage.show();
        
        // Barra izquierda y derecha
        Rectangle jugador1 = new Rectangle (ANCHO_JUGADOR, ALTO_JUGADOR);
        jugador1.setFill(Color.CORAL);
        jugador1.setTranslateX(POS_X_JUGADOR1);
        jugador1.setTranslateY(POS_Y_JUGADOR1);
        root.getChildren().add(jugador1);        
        Rectangle jugador2 = new Rectangle (ANCHO_JUGADOR, ALTO_JUGADOR);
        jugador2.setFill(Color.BLACK);
        jugador2.setTranslateX(POS_X_JUGADOR2);
        jugador2.setTranslateY(POS_Y_JUGADOR2);
        root.getChildren().add(jugador2);
        //marcador
        Label puntuacion1 = new Label(String.valueOf(0));
        puntuacion1.setTranslateX(WORLD_WIDTH*0.35);
        puntuacion1.setTextFill(Color.CORAL);
        puntuacion1.setFont(Font.font(25));
        root.getChildren().add(puntuacion1);
        
        Label puntuacion2 = new Label(String.valueOf(0));
        puntuacion2.setTranslateX(WORLD_WIDTH*0.65 - 10);
        puntuacion2.setTextFill(Color.BLACK);
        puntuacion2.setFont(Font.font(25));
        root.getChildren().add(puntuacion2);
        
        
        
        //  Barra del centro
        Rectangle lineaCentro = new Rectangle (anchoLineaCentro, AltoLineaCentro);
        lineaCentro.setFill(Color.WHITE);
        lineaCentro.setTranslateX(lineaX);
        lineaCentro.setTranslateY(lineaY);
        root.getChildren().add(lineaCentro);   
               
        // Bola
        Circle bola = new Circle (RADIO_BOLA);
        bola.setFill(Color.DARKORANGE);
        bola.setTranslateX(DIR_X_BOLA);
        bola.setTranslateY(DIR_Y_BOLA);
        root.getChildren().add(bola);
        
        new AnimationTimer() {
            @Override
            public void handle(long now) {
                
                double posX = bola.getTranslateX();
                double posY = bola.getTranslateY();
                bola.setTranslateX(posX + direccionHorizontal);
                bola.setTranslateY(posY + direccionVertical); 
       
                if(posX == 0){
                   
                     //direccionHorizontal = 2;
                }
                if(posX == 500){
                    
                     //direccionHorizontal = -1;
                }

                if(posY <= 0){
                    direccionVertical = 2;
                }
                if(posY >= 400){
                    direccionVertical = -2;
                }
                /*Para que rebote en el jugador 1 hemos sumado el ancho de la pala
                mas el radio de la bola, mas la gravedad y el alto pala que es el 30
                Y para el jugador 2 el radio mas la gravedad
                para cambiarle la direccion a la bola segun la zona en la que toque
                he partido la pala en tres partes de 0 a 10, de 11 a 20 y 
                de 21 a 30 y a la direccion le he cambiado el valor.
                */
                
                if(posX -12 <= jugador1.getTranslateX()){
                        if(posY >= jugador1.getTranslateY() && bola.getTranslateY()<= jugador1.getTranslateY()+10){
                      direccionHorizontal= 2;
                      direccionVertical= -2;
                        }  
                        if(posY >= jugador1.getTranslateY()+11 && bola.getTranslateY()<= jugador1.getTranslateY()+20){
                      direccionHorizontal= 2;
                        } 
                        if(posY >= jugador1.getTranslateY()+21 && bola.getTranslateY()<= jugador1.getTranslateY()+30){
                       direccionHorizontal= 2;
                       direccionVertical= 2;        
                        }
                    } 
                    if(posX +7 >= jugador2.getTranslateX()){
                        if(posY >= jugador2.getTranslateY() && bola.getTranslateY()<= jugador2.getTranslateY()+10){
                      direccionHorizontal= -2;
                      direccionVertical = -2;
                        } 
                        if(posY >= jugador2.getTranslateY()+11 && bola.getTranslateY()<= jugador2.getTranslateY()+20){
                      direccionHorizontal= -2;
                        }    
                        if(posY >= jugador2.getTranslateY()+21 && bola.getTranslateY()<= jugador2.getTranslateY()+30){
                       direccionHorizontal= -2;
                       direccionVertical= 2;        
                        }
                    } 
                //movimiento del jugador
                jugador1.setTranslateY( jugador1.getTranslateY() + gravedadJugador1);
                
                jugador2.setTranslateY( jugador2.getTranslateY() + gravedadJugador2);
                
                    if(jugador1.getTranslateY() <= 0){
                       gravedadJugador1 = 0;
                   
                    }
                      if(jugador1.getTranslateY() >=  WORLD_HEIGHT - ALTO_JUGADOR){
                       gravedadJugador1 = 0;
                    }
             
                      if(jugador2.getTranslateY() <= 0){
                       gravedadJugador2 = 0;
                   
                    }
                      if(jugador2.getTranslateY() >=  WORLD_HEIGHT - ALTO_JUGADOR){
                       gravedadJugador2 = 0;
                    }
                      if (posX < - RADIO_BOLA){
                        bola.setTranslateX(WORLD_WIDTH * 0.5);
                        puntuacion2.setText(String.valueOf(puntuacionJugador2++));
                    }
                      if (posX > WORLD_WIDTH + RADIO_BOLA){
                        bola.setTranslateX(WORLD_WIDTH * 0.5);
                        puntuacion1.setText(String.valueOf(puntuacionJugador1++));      
                }   
                      
                      
            }
        }.start();
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                double posPala1 = jugador1.getTranslateY();
                double posPala2 = jugador2.getTranslateY();
                
                switch (event.getCode()) {

                    case S:
                      if(posPala1 + gravedadJugador1 <= 0){
                        gravedadJugador1 = 0;
                         }else gravedadJugador1 = -3;
                            break;
                        
                    case X:
                      if(posPala1 + gravedadJugador1 + 30 >=  WORLD_HEIGHT ){
                        gravedadJugador1 = 0;
                         }else gravedadJugador1 = 3;
                            break;
                       
                    case UP:
                      if(posPala2 + gravedadJugador2 <= 0){
                        gravedadJugador2 = 0;
                         }else gravedadJugador2 = -3;                      
                            break;
                    
                    case DOWN:
                      if(posPala2 + gravedadJugador2 >= WORLD_HEIGHT - ALTO_JUGADOR){
                        gravedadJugador2 = 0;
                         }else gravedadJugador2 = 3;
                            break;
                
                }  
                
            }
        });
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
// Zona = (posicionBola - posPala) / 10