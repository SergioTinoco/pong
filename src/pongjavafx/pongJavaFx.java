/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pongjavafx;


import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 *
 * @author 1Daw02
 */
public class pongJavaFx extends Application {
    final int WORLD_WIDTH = 500;
    final int WORLD_HEIGHT = 250;
    
    @Override
    public void start(Stage primaryStage) {
       Group root = new Group();
        Scene scene = new Scene(root, WORLD_WIDTH, WORLD_HEIGHT, Color.AQUA);
        primaryStage.setTitle("pongJavaFx");
        primaryStage.setScene(scene);
        primaryStage.show();
        
        Rectangle barra = new Rectangle(15,125,5,30);
        barra.setFill(Color.CORAL);
        root.getChildren().add(barra);
        
        Circle bola = new Circle(200, 105,5);
        bola.setFill(Color.DARKBLUE);
        root.getChildren().add(bola);
        
         new AnimationTimer() {
            @Override
            public void handle(long now) {
               
                
                double posX = bola.getTranslateX();
                //velocidad de la bola, cambiar
                posX++; 
                bola.setTranslateX(posX);
            
                if(posX==250){
                    double posX1 = 125;
                    posX1--;
                    bola.setTranslateX(posX1); 
                }
            
            
            
            }
            
            
       
          }.start();
         }
         
           /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    }
                 

